#! /usr/bin/python
# Written by girados.

import serial
from pynmea import nmea

import threading
import datetime
import operator
import time
import sys
import signal


class gpsPrototype(threading.Thread):
  def __init__(self, arguments={}):
    threading.Thread.__init__(self)
    self.daemon = True
    keys = {
      'serial': {
        'port': '/dev/ttyUSB0',
        'timeout': 5,
        'baudrate': 9600
      }
    }
    keys.update(arguments)
    for key, value in keys.items():
      setattr(self, key, value)

    self.running = False
    self.connected = False
    self.fix = None

  def run(self):
      self.__connect()
      self.__read()
      self.__disconnect()

  def stop(self):
    self.__disconnect()

  def __connect(self):
    try:
      self.__conn = serial.Serial(self.serial['port'])
      self.__GPGGA = nmea.GPGGA()
    except Exception, e:
      print ("error: %s" % e)
      quit(1)
    self.connected = True

  def __disconnect(self):
    if self.connected:
      self.__conn.close()
      self.connected = False

  def __read(self):
    self.running = True
    while self.connected and self.running:
      line = self.__conn.readline()
      call = line[0:6]
      if '$GPGGA' in call:
        self.__GPGGA.parse(line)
        if getattr(self.__GPGGA, 'timestamp', None):
          self.fix = self.__fix(self.__GPGGA)

  class __fix:
    def __init__(self, values):
      self.lat_dec = self.__2dec(values.latitude, values.lat_direction)
      self.latitude = values.latitude
      self.lat_direction = values.lat_direction
      self.lon_dec = self.__2dec(values.longitude, values.lon_direction)
      self.longitude = values.longitude
      self.lon_direction = values.lon_direction
      self.num_sats = values.num_sats
      self.gps_qual = values.gps_qual
      self.horizontal_dil = values.horizontal_dil
      self.antenna_altitude = values.antenna_altitude
      self.altitude_units = values.altitude_units
      self.geo_sep = values.geo_sep
      self.geo_sep_units = values.geo_sep_units
      self.timestamp = self.__2time(values.timestamp)

    def echo(self):
      attrs = sorted(vars(self).items(), key=operator.itemgetter(0))
      print '\033[6;3H'
      for (key, value) in attrs:
        print ('  %-50s : %s ' % (key, value))

    def __dict__(self):
      attrs = sorted(vars(self).items(), key=operator.itemgetter(0))
      return attrs

    def __2dec(self, value, direction):
      dec = None
      dPos = value.find(".")
      if dPos > 0:
        mPos = dPos - 2
        ePos = dPos
        main = float(value[:mPos])
        min1 = float(value[mPos:])
        dec = float(main) + (float(min1) / float(60))
        if direction == "W":
          dec = -dec
        elif direction == "S":
          dec = -dec

      return dec

    def __2time(self, value):
      n = 2
      time = ':'.join([value[i:i + n] for i in range(0, len(value), n)][0:3])
      return time


def signalHandler(signal, frame):
  global gps
  if gps.running:
    gps.stop()
  sys.exit(1)

signal.signal(signal.SIGINT, signalHandler)
gps = gpsPrototype()
gps.start()
while gps.is_alive():
  if gps.fix and gps.running:
    gps.fix.echo()
    time.sleep(4)
