# README #

# Using the raspberry pi as a GPS Receiver. #
Use this python code to connect a gps via a usb / com port on your raspberry pi.
![raspberrypi+gps.jpg](https://bitbucket.org/repo/a6GoaB/images/2013775702-raspberrypi+gps.jpg)
# PREREQUISITES #
```
#!python
import serial
from pynmea import nmea
``` 
## [pyserial](https://github.com/pyserial/pyserial) ##
> This module encapsulates the access for the serial port. It provides backends for Python running on Windows, OSX, Linux, BSD (possibly any POSIX compliant system) and IronPython. The module named "serial" automatically selects the appropriate backend.

## [pynmea](https://code.google.com/p/pynmea/) ##
> This is a Python library for parsing NMEA data into usable objects.
> The idea of this project is to provide a general purpose approach to parsing NMEA data. Most other python libraries that deal with the data seem geared towards GPS only, leaving little room for developers to extend the library to their own needs.

# EXAMPLE #
On the example,signalHandler is used to catch **Control+C** and to stop gpsPrototype correctly.

Then create and start object class gpsPrototype, and use a while to communicate and print gps coordinates.
```
#!python
signal.signal(signal.SIGINT, signalHandler)
gps = gpsPrototype()
gps.start()
while gps.is_alive():
  if gps.fix and gps.running:
    gps.fix.echo()
    time.sleep(4)
```

# SOURCE #
gpsPrototype's git repo is available on bitbucket, which can be browsed at [bitbucket/girados/gpsprototype/source)](https://bitbucket.org/girados/gpsprototype/src) and cloned like that:


```
#!bash

git clone git@bitbucket.org:girados/gpsprototype.git
```


# DOCUMENTATION #
get more info about technologies on this links:

[Aprs](http://aprs.gids.nl/nmea/), nmea calls.

[pySerial](https://github.com/pyserial/pyserial), serial port library on python for linux, windows, ...

[pyNmea](https://pynmea.readthedocs.org/en/latest/), python nmea calls integration.

[wifi-walk](http://wifi-walk.tumblr.com/), python project with gpsPrototype implementation.


